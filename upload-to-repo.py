# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.
#!/usr/bin/python

import json
import requests
import os

jenkins_token = os.getenv("JENKINS_TOKEN")
jenkins_user = os.getenv("JENKINS_USER")
token = os.getenv("CI_JOB_TOKEN")
project_id = os.getenv("CI_PROJECT_ID")
project_name = os.getenv("CI_PROJECT_NAME")
pipeline_id = os.getenv("CI_PIPELINE_ID")
project_path = os.getenv("CI_PROJECT_PATH")

headers = {
    "JOB-TOKEN": token,
    "Accept": "application/json",
    "Content-Type": "application/json",
}

response =  requests.get((f"https://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline_id}/jobs"), headers=headers)
jobs = response.json()
for job in jobs:
    name=job["name"]
    job_id=job["id"]
    if name.startswith("build"):
        artifact_url = (f"https://gitlab.com/{project_path}/-/jobs/{job_id}/artifacts/download")
        print(artifact_url)
        response=requests.post("https://ci.linaro.org/job/upload-deb-package/buildWithParameters",
                auth=(jenkins_user, jenkins_token),
                data={'source' : artifact_url,
                      'zip': 'true'})

